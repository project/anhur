/**
 * @file
 * This file is to add any custom js for the anhur sub-theme.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.anhurSubthemeBehavior = {
      // Perform jQuery as normal in here.
  };

})(jQuery);
